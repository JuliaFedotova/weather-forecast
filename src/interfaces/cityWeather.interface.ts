import type {Geo} from "@/interfaces/geo.interface";
import type {Weather} from "@/interfaces/weather.interface";

export interface CityWeather {
    geo: Geo;
    weather: Weather;
}
