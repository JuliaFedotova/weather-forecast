export interface Weather {
    "lat": number,
    "lon": number,
    "current": {
        "dt": number,
        "sunrise": number,
        "sunset": number,
        "temp": number,
        "feels_like": number,
        "pressure": number,
        "humidity": number,
        "dew_point": number,
        "uvi": number,
        "clouds": number,
        "visibility": number,
        "wind_speed": number,
        "wind_deg": number,
        "wind_gust": number,
        "weather": [
            {
                "icon": string
            }
        ]
    },

    "daily": {
       "pop": number
    }[],

}
