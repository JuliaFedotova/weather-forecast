export interface Geo {
    "lat": number,
    "lon": number,
    "country": string,
    "name": string,

    "state": string,
    "local_names": {
        "ru": string,
    },

}
