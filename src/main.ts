import { createApp } from 'vue'
import App from './App.vue'

// Dev mode
const app = createApp(App)

app.mount('#app')

function initWidget() {
    const tagName = 'weather-widget';

    const customTagsNodes = document.querySelectorAll(tagName);

    customTagsNodes.forEach(customTagNode => {
        createApp(App).mount(customTagNode)
    });
}

document.addEventListener('DOMContentLoaded', () => {
    const link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    document.head.appendChild(link);
    link.href = '../dist/style.css';

    initWidget()
})

