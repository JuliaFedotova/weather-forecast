import {ref} from 'vue';
import {api} from "@/api/api";
import type {CityWeather} from "@/interfaces/cityWeather.interface";

const weatherDataList = ref<CityWeather[]>([]);

export function useWeatherData() {
    async function getCurrentWeatherData() {
        navigator.geolocation.getCurrentPosition(async position => {
            const weatherData = await api.weather.getWeatherDate(position.coords.latitude, position.coords.longitude)
            const cities = await api.geo.getCities(position.coords.latitude, position.coords.longitude)

            weatherDataList.value.push({
                geo: cities[0],
                weather: weatherData
            })
        })
    }

    async function getWeatherData(citiesCords: {lat: number, lon: number}[]) {

        for(let i = 0; i < citiesCords.length; i++) {
            const weatherData = await api.weather.getWeatherDate(citiesCords[i].lat, citiesCords[i].lon);
            const cities = await api.geo.getCities(citiesCords[i].lat, citiesCords[i].lon);

            weatherDataList.value.push({
                geo: cities[0],
                weather: weatherData
            })
        }
    }

    return {
        weatherDataList,
        getCurrentWeatherData,
        getWeatherData
    }
}
