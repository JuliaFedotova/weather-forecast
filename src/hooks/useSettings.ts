import {ref} from 'vue';

const isOpen = ref(false);
const startSearch = ref(false)

export function useSettings() {
    async function openPopup() {
        isOpen.value = true;
    }

    return {
        isOpen,
        startSearch,
        openPopup,
    }
}
