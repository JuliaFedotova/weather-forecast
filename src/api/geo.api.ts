import type {Geo} from "@/interfaces/geo.interface";
import {api} from "@/api/api";

export const geoApi = {

    async getGeoDate(q: string, limit: number): Promise<Geo[]> {
        const params = {
            q,
            limit: limit.toString(),
            appid: '60577cb9823a9920ce99efd2d26ae635',
        }
        const result = '?' + new URLSearchParams(params).toString();

        const response = await fetch(`http://api.openweathermap.org/geo/1.0/direct` + result);
        return await response.json();

    },

    async getCities(lat: number, lon: number): Promise<Geo[]> {
        const params = {
            lat: lat.toString(),
            lon: lon.toString(),
            appid: '60577cb9823a9920ce99efd2d26ae635',
        }

        const result = '?' + new URLSearchParams(params).toString();
        const response = await fetch(`http://api.openweathermap.org/geo/1.0/reverse` + result);
        return await response.json();
    },

}
