import { weatherApi } from '@/api/weather.api';
import { geoApi } from '@/api/geo.api';

export const api = {
    weather: weatherApi,
    geo: geoApi,
}
