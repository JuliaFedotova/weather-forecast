import type {Weather} from "@/interfaces/weather.interface";

export const weatherApi = {

    async getWeatherDate(lat: number, lon: number): Promise<Weather> {
        const params = {
            lat: lat.toString(),
            lon: lon.toString(),
            appid: '60577cb9823a9920ce99efd2d26ae635',
            units: 'metric'
        }

        const result = '?' + new URLSearchParams(params).toString();

        const response = await fetch(`https://api.openweathermap.org/data/3.0/onecall` + result);
        return await response.json();

    },
}
